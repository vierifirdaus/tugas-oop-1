const PI = 3.14159265359
class circle{
    constructor(radius){
        this.radius=radius
    }
    getRadius(){
        return this.radius 
    }
    setRadius(newradius){
        this.radius=newradius
        return this.radius  
    }
    getArea(){
        return (PI * this.radius * this.radius).toFixed(2)
    }
    getCircumference(){
        return (2 * PI * this.radius).toFixed(2)
    }
}
const r1=new circle(10);

console.log(`Nilai jari jari awal sebesar ${r1.getRadius()}`)
console.log(`Niali jari jari menjadi ${r1.setRadius(20)}`)
console.log(`Luas lingkaran dengan jari jari ${r1.setRadius(20)} adalah ${r1.getArea()}`)
console.log(`Keliling lingkaran dengan jari jari ${r1.setRadius(20)} adalah ${r1.getCircumference()}`)

